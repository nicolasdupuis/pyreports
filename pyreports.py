# Authors: Nicolas Dupuis
# Licensed under GPL V3
# https://gitlab.com/nicolasdupuis/pyreports

import os, sys, re
import os.path as op
from pathlib import Path
import itertools as itto
import pandas as pd
from jinja2 import Environment, FileSystemLoader
from markdown import markdown

try:
    from weasyprint import HTML
except:
    import warnings
    warnings.warn("pyreport startup - Cannot import weasyprint. PDF export is not possible.")

import datetime as dt
import logging

# This maybe tricky to install. On Ubuntu I had to:
# sudo apt install python-dev gcc
# pip install plotly-orca psutils
import plotly.io as pio  # Render Figure in SVG
pio.renderers.default = "svg"

import codecs # read HTML
import webbrowser # open a web browser and display a local page

import utilities # PyReports utilities


class PyReports:

    '''
        Create statistical reports in Python, with an interface inspired by SAS Proc Report.
    '''

    def __init__( self
                 ,container
                 ,path
                 ,title = None
                 ,description = None
                 ,settings = {}
                ):

        # Deal with settings
        #-----------------------------------------------------------------------------

        # Define all settings, their defaults and valid values

        valid_container_settings = { 'logging_level'    : {'default': 'INFO', 'valid_values': ['DEBUG','INFO','WARNING']}
                                    ,'stylesheets'      : {'default': ['style', 'style_phuse']}
                                    ,'sas_encoding'     : {'default': 'utf-8'}
                                    ,'date_format'      : {'default': '%b %d %Y %H:%M:%S'}
                                    ,'values_alignment' : {'default': 'right',  'valid_values': ['left','right']}
                                    ,'labels_alignment' : {'default': 'center', 'valid_values': ['left','center','right']}
                                    ,'toc'              : {'default': True, 'valid_values': [True, False]}
                                    }

        self.valid_containee_settings = { 'display_date'          : {'default': False,    'valid_values': [True, False]}
                                         ,'groupby_on_same_table' : {'default': False,    'valid_values': [True, False]}
                                         ,'show_index'            : {'default': False,    'valid_values': [True, False]}
                                         ,'page_break'            : {'default': False,    'valid_values': [True, False]}
                                         ,'max_rows'              : {'default': None}
                                         ,'remap_cols'            : {'default': None}
                                        }

        self.valid_export_settings = { 'open_in_browser' : {'default': False, 'valid_values': [True, False]}
                                     }


        # Initialize vars
        #-----------------------------------------------------------------------------
        self.temp_files = []

        # Report is the main dict that contains all the tables and figures info
        self.report_id = 0
        self.report = {}

        # Container is a dict about the container itself
        self.container = {}
        self.container['name'] = container
        self.container['path'] = path

        if title != None:
            self.container['title'] = markdown(title)
        else:
            self.container['title'] = container

        self.container['description'] = description

        # Validate container settings, use default if necessary
        self.container['settings'], issues = utilities.validate_settings(settings, valid_container_settings)


        # Initialize log
        #-----------------------------------------------------------------------------

        logging.basicConfig( filename = op.join(path, f"{self.container['name']}.log")
                            ,filemode = "w"
                            ,format   = "%(levelname)8s - %(module)16s > %(funcName)14s: %(message)s"
                            ,level    = self.container['settings']['logging_level']
                            )
        logging.info(" ".join([ f"PyReports log initiated at {dt.datetime.now(dt.timezone.utc)},"
                               ,f"with logging level '{self.container['settings']['logging_level']}'."]
                              )
                     )
        logging.info(f"Calling script is '{sys.argv[0]}', in '{sys.path[0]}'")
        logging.info(f"Report name is '{self.container['name']}', titled '{self.container['title']}'.")
        logging.info(f"The target location is '{self.container['path']}'")

        # Log settings
        for setting in self.container['settings']:
            if setting != "stylesheets" :
                logging.debug(f"Settings- '{setting}' = {self.container['settings'][setting]}")
            else :
                for key, val in self.container['settings']['stylesheets'].items() :
                    logging.debug(f"Settings- 'stylesheets' element titled '{key}' includes:")
                    for lst in val :
                        logging.debug(f"Settings-   URL/File are {lst['url']}, {lst['file']}")
                        logging.debug(f"Settings-     css content is {repr(lst['content'])}")

        # Log setting issues
        if len(issues) > 0:
            for issue in issues:
                logging.warning(issue)


    def add_table( self
                  ,data
                  ,filter      = None
                  ,columns     = None
                  ,title       = None
                  ,subtitle    = None
                  ,description = None
                  ,footnotes   = None
                  ,remap       = None
                  ,settings    = {}
                 ):

        '''
            Add a new table to the container.
        '''

        # That's a new table
        self.report_id += 1

        self.report[self.report_id] = {}
        self.report[self.report_id]['request'] = {}

        # Load all arguments in the Report dict. We'll analyze those later
        self.report[self.report_id]['request']['type']        = 'table'
        self.report[self.report_id]['request']['data']        = data
        self.report[self.report_id]['request']['columns']     = columns
        self.report[self.report_id]['request']['filter']      = filter
        self.report[self.report_id]['request']['title']       = title
        self.report[self.report_id]['request']['subtitle']    = subtitle
        self.report[self.report_id]['request']['description'] = description
        self.report[self.report_id]['request']['footnotes']   = footnotes
        self.report[self.report_id]['request']['remap']       = remap
        self.report[self.report_id]['request']['settings']    = settings


    def add_figure( self
                   ,figure
                   ,title       = None
                   ,subtitle    = None
                   ,description = None
                   ,footnotes   = None
                   ,settings    = {}
                  ):

        '''
            Add a new PLOTLY figure to the container
        '''

        # OK, that's a new figure
        self.report_id += 1

        if str(type(figure)) == "<class 'plotly.graph_objs._figure.Figure'>" :

            self.report[self.report_id]                           = {}
            self.report[self.report_id]['request']                = {}
            self.report[self.report_id]['request']['type']        = "figure"
            self.report[self.report_id]['request']['title']       = title
            self.report[self.report_id]['request']['subtitle']    = subtitle
            self.report[self.report_id]['request']['description'] = description
            self.report[self.report_id]['request']['footnotes']   = footnotes
            self.report[self.report_id]['request']['settings']    = settings
            self.report[self.report_id]['temp_files']             = []
            self.report[self.report_id]['figure_img']             = ""

            try :
                # Export the figure to SVG, for PDF export purpose. Create a HTML tag for it, for later
                filename = op.join(self.container['path'], f"_temp_pyreports_{self.report_id}.svg")
                self.report[self.report_id]['request']['settings']['file'] = filename

                # Overwrite any 'file' setting from user with this standard temp filename. Pass remaining user settings to plotly.
                figure.write_image(**self.report[self.report_id]['request']['settings'])
                self.report[self.report_id]['figure_img'] = f"<img src='{Path(op.abspath(filename)).as_uri()}' />"
                self.temp_files.append(filename)
            except :
                logging.warning("Unable to write SVG image. Most likely Plotly cannot access Orca on your system.")

            # Export Plotly figure to HTML file > read that file > store content, for later
            filename = op.join(self.container['path'], f"_temp_pyreports_{self.report_id}.html")
            figure.write_html(filename, full_html=False)

            with codecs.open(filename, 'r', 'utf-8') as img_html:
                self.report[self.report_id]['figure_html'] = img_html.read()

            self.temp_files.append(filename)
            logging.debug(f"Just added new temp filename '{self.temp_files[-1]}'.")

        else:
            logging.error(f"Figure for '{title}' must be a Plotly object.")


    def export( self
               ,output_formats = 'PDF'
               ,settings = {}
              ):

        '''
            Create the PyReport and export it as permanent file.
        '''

        # Check Arguments and settings

        self.container['export_settings'], issues = utilities.validate_settings(settings, self.valid_export_settings)


        # Let's process all the requested tables and figures
        #------------------------------------------------------------------------------------------------------------------------------------------------
        for i in self.report:

            # Validate table settings, use default if necessary
            self.report[i]['settings'], issues = utilities.validate_settings(self.report[i]['request']['settings'],
                                                                             self.valid_containee_settings)

            # Log setting issues
            if len(issues) > 0:
                for issue in issues:
                    logging.warning(issue)

            # That's a table!
            if self.report[i]['request']['type'] == 'table':

                # Deal with the data
                #------------------------------------------------------------------------------------------------------------------------------------------------

                # User gave us a dataframe, let's take a copy to prevent its alteration
                if isinstance(self.report[i]['request']['data'], pd.DataFrame):
                    df = self.report[i]['request']['data'].copy()

                # OK, then let's convert that data into a pandas dataframe
                else:
                    df = utilities.convert_input_data(self.report[i]['request']['data'])


                # Analyze arguments passed by user for that table, and load the digested into the Report dictionary
                #-----------------------------------------------------------------------------------------------------------------------------------------
                self.report[i]['digest'] = utilities.digest_request(self.report[i]['request'], list(df.columns), i)


                # Filter data if requested
                #------------------------------------------------------------------------------------------------------------------------------------------------
                if self.report[i]['digest']['filter'] != None:

                    try:
                        df = df.query(self.report[i]['digest']['filter'])
                        df = df.copy() # to avoid subsequent warning when slicing

                    except Exception:
                        logging.warning(f"Report {self.report[i]['digest']['title']}: failed to apply filter on the Dataframe.", exc_info=True)


                # Compute new variables, if any requested
                #--------------------------------------------------------------------------------------------------------------------------------
                for column in self.report[i]['digest']['compute']:

                    compute = self.report[i]['digest']['column'][column]['compute']

                    # Replace variable names with a pandas syntax. For example: HEIGHT -> df['HEIGHT']
                    for token in re.findall('\w*', compute):

                        if token in df.columns:

                            compute = compute.replace(token, "df['" + token + "']")

                    try:
                        df[column] = eval(compute)
                    except Exception:
                        msg = (f"Sorry, I couldn't compute '{column}'. I'm gonna stop there.")
                        logging.error(msg, exc_info=True)
                        raise Exception (msg)


                # Summary statistics ('analysis' variables)
                # We have 4 scenarios: group or not, combined with display|order or not.
                #--------------------------------------------------------------------------------------------------------------------------------
                if len(self.report[i]['digest']['analysis']) > 0:

                    # Analysis with group
                    if len(self.report[i]['digest']['group']) > 0:

                        # No display|order: we can summarize the data, i.e. build a new dataframe from scratch with groups.
                        if  len(self.report[i]['digest']['display']) + len(self.report[i]['digest']['order']) == 0 :

                            # agg() applies all the descriptive functions we need.
                            # self.report[i]['digest']['analysis'] is a Dict that could like: {'AGE': 'mean', 'WEIGHT': 'max'}
                            df = df.groupby(self.report[i]['digest']['group']).agg(self.report[i]['digest']['analysis'])
                            df = df.reset_index()

                            # agg() output column names are the same as its input. We may have to rename this as per user request.
                            df = df.rename(columns=self.report[i]['digest']['rename'])

                        # We do have Display|order. Weird. We need to summarize the column(s), by subgroup, AND keep the structure of the input data
                        else:

                            df = df.set_index(self.report[i]['digest']['group'])

                            # For each analysis variable, set their value to the outcome of agg(), by subgroup.
                            for column in self.report[i]['digest']['rename'].values():

                                var_in = self.report[i]['digest']['column'][column]['input']
                                aggregation = self.report[i]['digest']['column'][column]['usage']

                                df[column] = df.groupby(self.report[i]['digest']['group']).apply(lambda group: group.agg( {var_in:aggregation} ) )

                            df = df.reset_index()

                    # Analysis without group
                    else:

                        # No display|order: let's just summarize the data
                        if len(self.report[i]['digest']['display']) + len(self.report[i]['digest']['order']) == 0:

                            # Create a new df from the outcome of agg() and let's transpose indexes, that'll look pretty.
                            df = pd.DataFrame(df.agg(self.report[i]['digest']['analysis'])).T

                            # agg() output column names are the same as its input. We may have to rename this as per user request.
                            df = df.rename(columns=self.report[i]['digest']['rename'])

                        # We do have display|order. Weird. We need to summarize the column(s) AND keep the structure of the input data
                        else:

                            for column in self.report[i]['digest']['rename'].values():

                                var_in = self.report[i]['digest']['column'][column]['input']
                                aggregation = self.report[i]['digest']['column'][column]['usage']

                                df[column] = df.agg( {var_in:aggregation})[var_in]


                # Create new categorical variables, if any requested
                #--------------------------------------------------------------------------------------------------------------------------------
                for column in self.report[i]['digest']['categories']:

                    # Cut parameters passed by user
                    cut_params = self.report[i]['digest']['column'][column]['categories']

                    # Let's build a command using all the parameters
                    cut = "pd.cut(df[self.report[i]['digest']['column'][column]['input']], "

                    for key in self.report[i]['digest']['column'][column]['categories']:

                        cut += key + "=" + str(cut_params[key]) + ","

                    cut += ")"

                    # Execute that command
                    try:
                        df[column] = eval(cut)
                    except:
                        msg = (f"Sorry, I couldn't create the categorical column '{column}'. I'm gonna stop there.")
                        logging.error(msg, exc_info=True)
                        raise Exception (msg)

                # Now we can drop all the dataframe variables we don't need
                #--------------------------------------------------------------------------------------------------------------------------------
                df = df [ self.report[i]['digest']['columns'] ].copy() # copy() prevents the ugly slice message in the log


                # Remap values
                #--------------------------------------------------------------------------------------------------------------------------------
                for column in self.report[i]['digest']['columns']:

                    if 'remap' in self.report[i]['digest']['column'][column]:

                        controlled_terminology = self.report[i]['digest']['column'][column]['remap']

                        # check if we can remap the variable
                        if controlled_terminology in self.report[i]['digest']['remap']:
                            df[column].replace(self.report[i]['digest']['remap'][controlled_terminology], inplace=True)

                        else:
                            logging.warning(f"I cannot remap variable '{column}' because of your remaping dictionary")

                # Order dataframe (using groups and then ordered vars)
                if len(self.report[i]['digest']['orders']['columns']) > 0:

                    df.sort_values(self.report[i]['digest']['orders']['columns'],
                                       ascending = self.report[i]['digest']['orders']['ascending'],
                                       inplace   = True)

                # Transpose values, aka Across
                #--------------------------------------------------------------------------------------------------------------------------------
                if len(self.report[i]['digest']['across']) > 0:

                    # Group the dataframe and count occurences per subgroup
                    df = df.groupby(self.report[i]['digest']['group'] + self.report[i]['digest']['across']).size().reset_index()

                    # Transpose the across variable values
                    df = df.pivot_table(index=self.report[i]['digest']['group'], values=0,
                                        columns=self.report[i]['digest']['across']).reset_index().fillna(0)

                    # Massage the dataframe to get a nice spanning header (i.e. a hierchical column index)
                    across_columns = ['' for n in range(len(self.report[i]['digest']['group']))] + list(df.columns)
                    for column in self.report[i]['digest']['group']:
                        across_columns.remove(column)
                    df = pd.DataFrame(df.values, columns=[ self.report[i]['digest']['group'] + [ self.report[i]['digest']['across'][0] for n in range(len(df.columns) - len(self.report[i]['digest']['group']))], across_columns])

                    # For later, we won't follow the normal route
                    self.report[i]['digest']['group'] = []

                # Set Width (char variables) and precision (numeric variables)
                #--------------------------------------------------------------------------------------------------------------------------------
                for column in self.report[i]['digest']['column']:

                    # Resize strings using width info
                    if 'width' in self.report[i]['digest']['column'][column]:

                        if df[column].dtype == 'object':
                            df[column] = df[column].str.slice(start=0, stop=self.report[i]['digest']['column'][column]['width'])

                        else:
                            logging.warning(f'Cannot change width of numeric variable {column}.')

                    # Set precision (number of digits)
                    if 'digits' in self.report[i]['digest']['column'][column]:

                            if df[column].dtype in ['float64','int64']:
                                _format = '%2.' + str(self.report[i]['digest']['column'][column]['digits']) + 'f'
                                df[column] = df[column].map(lambda x: _format % x)

                            else:
                                logging.warning(f'Cannot apply precision on character variable {column}.')


                # OK, our dataframe is now ready!

                # Convert Dataframe to HTML
                #------------------------------------------------------------------------------------------------------------------------------------------------

                def to_html(df, show_index=False):

                    # Determine if we have a max amount of rows to print out
                    if self.report[i]['settings']['max_rows'] == None:
                        max_rows = len(df)
                    else:
                        max_rows = self.report[i]['settings']['max_rows']

                    return df.to_html(  na_rep    = ''                               # String representation for NaN
                                       ,index     = show_index
                                       ,bold_rows = True
                                       ,justify   = self.container['settings']['labels_alignment']
                                       ,decimal   = '.'
                                       ,border    = 10
                                       ,classes   = 'aligned_df'
                                       ,max_rows  = max_rows
                                       #,sparsify   = True  # Set to False for a df with a hierarchical index to print every multiindex key at each row.
                                    )

                # No grouping required (or already done with 'across')
                if len(self.report[i]['digest']['group']) == 0 or len(self.report[i]['digest']['across']) > 0 :

                    # Rename Dataframes columns
                    df.rename(columns=self.report[i]['digest']['labels'], inplace=True)

                    # Display row index if we're using default columns. Don't show otherwise
                    df_html = to_html(df, show_index=self.report[i]['settings']['show_index'])

                # We need to regroup values
                else:

                    # All groups in the same table (play with index)
                    if self.report[i]['settings']['groupby_on_same_table'] == True:

                        # Rename columns
                        df = df.set_index(self.report[i]['digest']['group']).rename(columns=self.report[i]['digest']['labels'])

                        # Rename index
                        df.index.names = [self.report[i]['digest']['labels'][old] for old in df.index.names]

                        # Convert to HTML
                        df_html = to_html(df, True)

                        # for Jinja2 template to know what to print
                        self.report[i]['digest']['group'] = []

                    # A subtable for each subgroup
                    else:

                        # Let's build a dictionary of grouped dataframes & names
                        df_html = {}

                        for name, group in df.groupby(self.report[i]['digest']['group']):

                            # To avoid subsequent warning when slicing
                            df = group.copy()

                            # Drop group column.
                            df.drop(self.report[i]['digest']['group'], axis=1, inplace=True)

                            # Rename Dataframes columns
                            df.rename(columns=self.report[i]['digest']['labels'], inplace=True)

                            df_html[name] = {}

                            # Groupby created 'name' as either a string (1 group) or a tuple of strings (multiple groups).
                            # Thank you Pandas but let's rationalize this
                            if isinstance(name, str):
                                    name = (name)

                            df_html[name]['name'] = name
                            df_html[name]['html'] = to_html(df)

                self.report[i]['table'] = df_html

            # It's a figure!
            elif self.report[i]['request']['type'] == 'figure':

                # Analyze arguments passed by user for that table, and load the digested into the Report dictionary
                #-----------------------------------------------------------------------------------------------------------------------------------------
                self.report[i]['digest'] = utilities.digest_request(self.report[i]['request'], None, i)


        # Render Jinja2 template
        #------------------------------------------------------------------------------------------------------------------------------------------------

        # Initialize Jinja2 environement
        env = Environment(loader=FileSystemLoader('.'))

        # Load the Jinja2 template
        template = env.get_template('template.html')

        # Fill out the template placeholders
        placeholders = { 'container'          : self.container
                        ,'report'             : self.report
                        ,'date'               : dt.datetime.now().strftime(self.container['settings']['date_format'])
                    }

        # Render!
        jinja2 = template.render(placeholders)

        logging.debug('Jinja2 template rendered.')


        # Analyse output_format argument and set to default if necessary
        #------------------------------------------------------------------------------------------------------------------------------------------------
        valid_values = ['HTML', 'PDF']

        # If user passed a string, let's accept this for now, as a list
        if isinstance(output_formats, str):
            output_formats = [output_formats]

        # Check if all items in the list are valid
        if isinstance(output_formats, list):
            for fmt in output_formats:
                if fmt not in valid_values:
                    logging.warning(f"Output format {fmt} is invalid. Setting the default value (i.e. ['PDF'] ).")
                    output_formats.remove(fmt)

            if output_formats == None: # we removed all...
                output_formats = valid_values
        else:
            logging.warning(f"Output format is invalid. Setting the default value (i.e. ['PDF'] ).")
            output_formats = valid_values


        # Final export
        #------------------------------------------------------------------------------------------------------------------------------------------------

        logging.info("-" * 100)

        # Lists of STYLESHEET urls, filenames and CSS content for dynamic and static outputs.
        # Extracted and "flattened" using itertools.chain(), to get 1-level lists of non-missing items.
        # See digest_stylesheets.py for structure of self.container['settings']['stylesheets']
        # "file" and "content" lists are ALWAYS same length, to write /css/ files used by DYNAMIC reports, like HTML
        # "url" list of "Persistent" and "Preferred" stylesheets is for FIXED reports, like PDF
        css_fi = [ [itm['file'] for itm in val if '' != itm['file']]
                    for key, val in self.container['settings']['stylesheets'].items() ]
        css_fi = list(itto.chain(*css_fi))
        css_pa = [op.join(self.container['path'], "css", itm) for itm in css_fi]

        css_co = [ [itm['content'] for itm in val if '' != itm['content']]
                    for key, val in self.container['settings']['stylesheets'].items() ]
        css_co = list(itto.chain(*css_co))

        css_ur = [ [itm['url'] for itm in val if '' != itm['url']]
                    for key, val in self.container['settings']['stylesheets'].items() if key in ["", "Preferred"] ]
        css_ur = list(itto.chain(*css_ur))

        logging.debug(f"Ready to write these /css/ files: {css_pa}.")

        if len(css_fi) != len(css_co) :
            logging.warning(f"Unexpected unequal lengths of filename (len {len(css_fi)}) and css-content (len {len(css_co)}) lists.")

        # HTLM Export and open in a web-browser
        if 'HTML' in output_formats:

            jinja2_final = jinja2

            # Replace Figure placeholder
            for i in self.report:

                if self.report[i]['request']['type'] == 'figure':

                    # search for @@figure_id=i@@ in the Jinja2 rendition and replace with the actual HTLM code
                    jinja2_final = jinja2_final.replace(f"@@figure_id={str(i)}@@", self.report[i]['figure_html'])

            filename = op.join(self.container['path'], self.container['name'] + ".html")

            try :
                with open(filename, 'w') as export_file :
                    export_file.write(jinja2_final)

                # Create folder for local /css/ files, as requested
                try:
                    Path(op.join(self.container['path'], 'css')).mkdir(parents=True, exist_ok=False)
                except FileExistsError :
                    logging.debug(f"HTML- Folder 'css' exists in '{self.container['path']}'.")
                except :
                    logging.warning(" ".join( f"HTML- Unable to create 'css' folder in '{self.container['path']}'."
                                             ,"Cannot create external css files."))

                try :
                    for pa, co in zip(css_pa, css_co) :
                        with open(pa, 'w') as nxtfile :
                            nxtfile.write(co)
                    logging.debug(f"HTML- Wrote to stylesheet {pa}.")
                except :
                    logging.warning("".join( f"HTML- Unable to create all files in dir "
                                            ,f"{op.join(self.container['path'], 'css')}."
                                            ,"Some styles will be missing.")
                                            )

                # Open the Web browser and that HTLM page
                if self.container['export_settings']['open_in_browser'] == True:
                    webbrowser.open_new_tab(filename)

                logging.info(f"HTML- Report '{self.container['name']}' was exported as HTML.")
            except :
                logging.warning(f"HTML- Unable to create '{filename}'. Ensure that this is writable.")

        # PDF export
        if 'PDF' in output_formats:

            # Replace Figure placeholder
            jinja2_final = jinja2

            # Replace Figure placeholder
            for i in self.report:

                if self.report[i]['request']['type'] == 'figure':

                    logging.debug(f"PDF- Placing into PDF temp SVG image {self.report[i]['figure_img']}.")
                    try :
                        # search for @@figure_id=i@@ in the Jinja2 rendition and replace with the actual HTLM code
                        jinja2_final = jinja2_final.replace(f"@@figure_id={str(i)}@@", self.report[i]['figure_img'])

                    except :
                        logging.warning("PDF- Unable to add image to PDF. Most likely Plotly Orca is not available on your system.")

            try :
                # Export to an HTML object (for conversion purpose)
                html_export = HTML(string=jinja2_final, base_url=self.container['path'])

                # and write a PDF
                filename = op.join(self.container['path'], self.container['name'] + ".pdf")

                # Pass Persistent and Preferred stylesheets to PDF export
                logging.debug(f"PDF- STYLESHEETS urls passed to PDF: {css_ur}.")
                html_export.write_pdf(target=filename, stylesheets=css_ur)
                logging.info(f"PDF- Report '{self.container['name']}' was exported as PDF.")
            except :
                logging.warning(f"PDF- Cannot write to '{self.container['name']}.pdf'. Ensure WeasyPrint is available, and the file is writable.")

        # Clean my mess
        for file in self.temp_files:
            os.remove(file)

        # TEMPORARY: debug CSS with https://weasyprint.readthedocs.io/en/stable/tutorial.html

        # That's a wrap
        print(f"PyReports Export complete. Check for WARNINGs and ERRORs in the log file {self.container['name']}.log.")
        logging.info(f"Report '{self.container['name']}' has been exported to '{self.container['path']}' in your formats.")
        logging.info(f'Logging terminated at {dt.datetime.now(dt.timezone.utc)}.')
        logging.info(f'https://gitlab.com/nicolasdupuis/pyreports')
