import pandas as pd
import random

# Remapping dictionary (aka. Controled terminology/codelists/formats)
remapping = {'treatment': {1: 'Whateverab (low dose)',
                           2: 'Whateverab (high dose)',
                           3: 'Placebo'}
            }

df_remap = pd.DataFrame( {'ct': ['treatment', 'treatment', 'treatment'],
                          'old': [1,2, 3],
                          'new': ['Whateverab (low dose)', 'Whateverab (high dose)', 'Placebo']
                           }
                        )
# Demographics
#--------------------------------------------------------------------------------------------

def create_dm(n_patients = 10):

   genders = ['Male', 'Female']
   races = ['White', 'Black', 'Asian']
   country = ['Switzerland', 'France', 'Belgium']
   treatments = [1, 2, 3]

   dm = pd.DataFrame( {'SUBJID': range(10010, 10010+ n_patients),
                     'SEX'     : [random.choice(genders) for pt in range(n_patients)],
                     'AGE'     : [random.randint(18, 65) for pt in range(n_patients)],
                     'WEIGHT'  : [random.randint(50, 100) for pt in range(n_patients)],
                     'HEIGHT'  : [random.randint(14, 18)/10 for pt in range(n_patients)],
                     'RACE'    : [random.choice(races) for pt in range(n_patients)],
                     'TRT'     : [random.choice(treatments) for pt in range(n_patients)],
                     'COUNTRY' : [random.choice(country) for pt in range(n_patients)],
                     },
                        columns=['SUBJID', 'SEX', 'AGE', 'WEIGHT', 'HEIGHT', 'RACE', 'TRT','COUNTRY']
                     )
   dm.name = 'dm'

   return dm


# Vital signs
#--------------------------------------------------------------------------------------------

def create_vs():
   patients = [10010, 10011, 10013]
   visits = [1, 2, 3]
   param = ['Heart rate', 'Systolic blood pressure']

   data = {'SUBJID': sorted(patients * len(visits)) * len(param),
         'VISIT' : visits * len(param) * len(patients),
         'PARAM' : sorted(param * len(visits) * len(patients)),
         'RESULT': [random.randint(50, 150)  for n in range(len(visits) * len(patients))] +
                     [random.randint(100, 180) for n in range(len(visits) * len(patients))] 
   }

   vs = pd.DataFrame(data, columns=['SUBJID', 'VISIT', 'PARAM', 'RESULT']).sort_values(['SUBJID','VISIT', 'RESULT'])

   return vs