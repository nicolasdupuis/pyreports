# Authors: Nicolas Dupuis (2020)
# https://gitlab.com/nicolasdupuis/pyreports
# GPL V3
#
# Practical applications of PyReports

import pandas as pd
import plotly.express as px
import sys
sys.path.append(".")

from pyreports import PyReports
from examples.testdata import create_dm, create_vs, remapping, df_remap

# Create some random demographic data for our examples
dm = create_dm(n_patients=20)
age_mean = pd.DataFrame(dm.groupby('SEX')['AGE'].describe())
age_mean.name = "Age average by sex"

#------------------------------------------------------------------------------------------------------------------------
# Let's first create a new report container 'demog'. We must give a name to it and a path where to save it.
# We can optionnaly add a description and modify certain settings.
#------------------------------------------------------------------------------------------------------------------------

demog = PyReports( 'PyReports_examples'
                  ,'./examples'
                  ,title = "__PyReports__ examples"
                  ,description = "This show cases the different features of PyReports."
                  ,settings = { 'stylesheets': { "": 'style'
                                                ,'Preferred': ['style_phuse', 'style_phuse_dkgreen']
                                                ,'PhUSE Light Green': ['style_phuse', '../css/style_phuse_ltgreen.css']
                                                ,'W3C Core Swiss': {"url": 'https://www.w3.org/StyleSheets/Core/Swiss'}
                                              }
                               ,'logging_level': 'INFO'
                              }
                 )


#------------------------------------------------------------------------------------------------------------------------
# Now let's add a table into that container.
# Sample report: quick dump of all the columns in a table. Title will be defaulted to the dataframe name, if any.
#------------------------------------------------------------------------------------------------------------------------

demog.add_table(dm)

# ------------------------------------------------------------------------------------------------------------------------
# We can add as many report item as we want. Let's add a second one.
# Sample report: Add title/subttile/footnotes, filter data, simple list of columns we want
#------------------------------------------------------------------------------------------------------------------------

footnotes = "Powered by [Pyreport](https://gitlab.com/nicolasdupuis/pyreports)"

demog.add_table(  data     = dm
                , filter    = "COUNTRY =='France'"
                , title     = 'Demographic report DM02'
                , subtitle  = 'French population'
                , columns   = ['SUBJID', 'AGE', 'SEX', 'RACE']
                , footnotes = footnotes
                , settings  = {'display_date': True} # overriding default settings
                )

#------------------------------------------------------------------------------------------------------------------------
# Sample report: Provide info on how to handle columns (grouping, labels, numeric precision, remap values)
#------------------------------------------------------------------------------------------------------------------------

description = '''
Lorem ipsum dolor sit amet, __consectetur adipiscing__ elit.

Hard Returnus ab morbi mi ante, tinciduntquis  
et softimus returnium vehicula sed, vehicula sed augue.
'''

demog.add_table(  data       = dm
                ,title       = 'Demographic report DM03'
                ,subtitle    = 'Optional subtitle'  
                ,description = description
                ,columns     = { 'SUBJID': {'usage': 'display', 'label': 'Subject ID'}
                                ,'SEX'   : {'usage': 'group',   'label': 'Sex'}
                                ,'AGE'   : {'usage': 'order',   'label': 'Age', 'ascending': False, 'digits': 1 }
                                ,'WEIGHT': {}
                                ,'TRT'   : {'usage': 'display', 'label': 'Treament', 'remap': 'treatment' }
                                }            
                ,remap       = df_remap
                ,footnotes   = footnotes
                ,settings    = {'groupby_on_same_table':False}
                )  

# ------------------------------------------------------------------------------------------------------------------------
# Sample report: Transpose values (across'). Across variable is transposed and occurences in each group are counted
#------------------------------------------------------------------------------------------------------------------------

description   = "Example with a remapped *across* variable (Treatment) and 2 groups (Race and Sex). "
description +=  "By default, the *across* variable is transposed and occurences in each group are counted. "
description +=  "Note that we could remap the values of the across variable, before the transpose."

demog.add_table(  data       = dm
                ,title       = 'Demographic report __DM04__'
                ,description = description
                ,columns     = { 'RACE' : {'usage': 'group',  'label': 'Race'}
                                ,'SEX'  : {'usage': 'group',  'label': 'Sex'}
                                ,'TRT'  : {'usage': 'across', 'label': 'Treatment', 'remap': 'treatment'}
                               }
                ,remap       = remapping
                )

# ------------------------------------------------------------------------------------------------------------------------
# Sample report: Produce summary statistics 
#------------------------------------------------------------------------------------------------------------------------

description   = "Example with descriptive statistics (mean and max) applied on variables (AGE and WEIGHT), by subgroups."

demog.add_table(  data       = dm
                ,title       = 'Demographic report __DM05__'
                ,description = description
                ,columns     = {'RACE'    : {'usage': 'group',                'label': 'Race'}
                               ,'AGE'    :  {'usage': 'mean',                 'label': 'Mean Age', 'digits':1}  # Doing so will implicitely use and replace AGE
                              #,'AGE_mean': {'usage': 'mean', 'input': 'AGE', 'label': 'Mean age', 'digits':1}  # You can explicitely use a variable without replacing it
                               ,'WEIGHT'  : {'usage': 'max',                  'label': 'Max weight'}
                               }
                ,settings    = {'groupby_on_same_table':True}                                   
                )

#------------------------------------------------------------------------------------------------------------------------
# Sample report: Summary statistics, cont.
#------------------------------------------------------------------------------------------------------------------------

demog.add_table( data        = dm
                ,title       = 'Demographic report __DM06__'
                ,columns     = { 'SEX'     : {'usage': 'group',}  # Try out different scenarios (enable or not)
                                 #,'SUBJID' : {'usage': 'display'} # Try out different scenarios (enable or not) 
                                 ,'WEIGHT'  : {'usage': 'mean',   'label': 'Average Weight', 'digits':1 }
                                 ,'HEIGHT'  : {'usage': 'median', 'label': 'Median Height', 'digits':1 }
                                }
                ,description = 'Regarding summary statistics, PyReports can handle four scenarios depending on: \n\n' +\
                               '* whether there are groups or not \n' +\
                               '* whether there are display|order variables or not'
                )

#------------------------------------------------------------------------------------------------------------------------
# Sample report: Derive a new variable (compute)
#------------------------------------------------------------------------------------------------------------------------

demog.add_table( data        = dm
                ,title       = 'Demographic report __DM07__'
                ,columns     = { 'SUBJID' : {'usage': 'display', 'label': 'Subject ID'}
                                ,'BMI'    : {'usage': 'display', 'compute': 'WEIGHT / (HEIGHT**2)', 'label': 'Body Mass Index', 'digits':1 }
                               }
                ,description = 'Example with a computed variable. Body Mass Index is calculated from WEIGHT and HEIGHT. Only 10 rows are printed out.'
                ,settings    = {'max_rows': 10}
                )  

#------------------------------------------------------------------------------------------------------------------------
# Sample report: Derive a new variable and produce summary statistics on it
#------------------------------------------------------------------------------------------------------------------------

demog.add_table( data        = dm
                ,title       = 'Demographic report __DM08__'
                ,columns     = { 'SEX'     : {'usage': 'group', 'label': 'Sex'}
                                ,'MEAN_BMI': {'usage': 'mean', 'compute': 'WEIGHT / (HEIGHT**2)', 'label': 'Average BMI', 'digits':2 }
                               }
                ,description = 'We can produce summary statistics on a computed variable.'
                ,settings    = {'groupby_on_same_table':True}
                )

#------------------------------------------------------------------------------------------------------------------------
# Sample report: Create a categorical variable, follows the logic of pandas cut()
#------------------------------------------------------------------------------------------------------------------------

bins   = [17, 30, 50, 65]
labels = ['Young adult', 'Middle-aged', 'Elder'] # 18-30, 30-50 and 50-65

demog.add_table( data        = dm
                ,title       = 'Demographic report __DM09__'
                ,columns     = { 'SUBJID' : {'usage': 'display', 'label': 'Subject ID'}
                                ,'AGE'    : {'usage': 'order'  , 'label': 'Age'}
                                ,'AGE_cat': {'usage': 'display', 'input': 'AGE', 'categories': {'bins': bins, 'labels': labels}, 'label': 'Age category'}
                               }
                ,description = "Produce a categorical variable following the logic of pandas cut() function."
                )

# Note1: In 'categories' you can pass all the possible parameters of the pandas cut() function. Example:
# 'AGE': {'usage': 'display', 'categories': {'bins': bins, 'right': True} } # In the absence of 'input', variable AGE is used and replaced!
# cut(): https://pandas.pydata.org/pandas-docs/version/0.23.4/generated/pandas.cut.html

#  Note2: You can create a categorical variable on summary statistics. Example:
# 'AGE': {'usage': 'mean', 'categories': {'bins': bins, 'labels': labels}} # In the absence of 'input', variable AGE is used and replaced!


#------------------------------------------------------------------------------------------------------------------------
# Interface exploration: new summarizing column (sum, mean, etc), element-wise from a set of columns
# 
# Example with Total and Mean. Mean_A is a 'regular' mean, column-wise.
#
#  A    B     Total   Mean  Mean_A
#  2    6     8       4      3.3
#  0    6     6       3      3.3
#  8    10    18      9      3.3
# #------------------------------------------------------------------------------------------------------------------------

# demog.add_table( data        = 
#                 ,title       = ''
#                 ,columns     = { 'A'       {'usage': 'display'}
#                                 ,'B':      {'usage': 'display'}

#                                 ,'Total':  {'usage': 'sum',     'input': ['A','B']} -> element-wise, keep dimension  
#                identical to:    ,'Total':  {'usage': 'display', 'compute': 'A+B'}   -> OK here but maybe not so with more complex stats

#                                 ,'Mean':   {'usage': 'mean',    'input': ['A','B']} -> element-wise, keep dimension
#                identical to     ,'Mean':   {'usage': 'mean'}                        -> absence of input means take all columns, if column name doesn't exist :s

#                                 ,'Mean_A': {'usage': 'mean',    'input': 'A'}       -> column-wise, reduce dimension. If input is not provided, column is used and replaced.

#                therefore :      ,'A':      {'usage': 'mean',    'input': ['A','B']} -> should be illegal.
#                                }
#                 ,description = ""
#                 )


#------------------------------------------------------------------------------------------------------------------------
# Interface exploration: summarizing row (sum, mean, etc), column-wise
# 
# Example with A & B: new rows 'mean' & 'sum'.
#
#           A    B    
#          ---  ---
#           2    5    
#           0    6    
#           8    10   
#  Average  5    
#  Total    10   21
# #------------------------------------------------------------------------------------------------------------------------

# demog.add_table( data        = 
#                 ,title       = ''
#                 ,columns     = { 'A': {'usage': 'display', 'row_summary': {stats: ['mean', 'sum'], label: ['Average', 'Total']}}
#                                 ,'B': {'usage': 'display', 'row_summary': {stats: 'sum',           label: 'Total'}} 
#                                }
#                 ,description = ""
#                 )


#------------------------------------------------------------------------------------------------------------------------
# Interface exploration: group|order|across on new stats
#------------------------------------------------------------------------------------------------------------------------

# Right now the way to summarize a variable, columns-wise, is: 

# demog.add_table(  data       = dm
#                 ,columns     = {'RACE': {'usage': 'group'}
#                                ,'AGE' : {'usage': 'mean', 'label': 'Mean Age'}
#                                })

# It's easy, elegant and dense, but the consequence of using Usage is that we lose the ability to group|order|across what comes out of the summary stats. 
# Proc Report forces you to repeat yourself (column+define), but allows full flexibility. The current PyReports syntax is easier but that comes with a price.
# So, what do we do ? Do we even need an improvement? Is there a scenario where that would be useful?
# Related to that, it could be useful to have a way to apply a function, of any kind, on a given variable, with of course a list of (key) arbritrary arguments.
# Our cut() could work that way, and we could generalize it. That seems quite Pythonic to me. This prob will make the simple syntax is bit more verbose

#------------------------------------------------------------------------------------------------------------------------
# Sample figure: You can pass a Plotly figure to the container
#------------------------------------------------------------------------------------------------------------------------

# Create a fancy Plotly figure
df = px.data.iris()
fig = px.scatter(df, x="sepal_width", y="sepal_length", color="species", facet_col="species", trendline="ols")
fig.update_layout(title_font_size=24, height=700, width=1000)
fig.update_xaxes(showgrid=False)
fig.update_traces(line=dict(dash="dot", width=4), selector=dict(type="scatter", mode="lines"))

# Add that figure to the container
demog.add_figure ( title       = "Fancy figure"
                  ,description = "You can add a __Plotly__ figure to a PyReport. To be displayed in a PDF, the figure is rendered in SVG first. " + \
                                 "The Plotly figure keeps its interactivity in the HTML export."
                  ,figure      = fig
                  ,settings    = {"width": 600, "height": 600})

#------------------------------------------------------------------------------------------------------------------------
# Export the container
#------------------------------------------------------------------------------------------------------------------------

# Give a list of export format. Available: HTML (opens your browser) and PDF (saves a file)

#demog.export(['PDF','HTML'])
#demog.export('PDF')
demog.export(['PDF','HTML'], settings = {'open_in_browser': True})
