# PyReports

This package creates statistical reports in Python, with an interface inspired by SAS(r) Proc Report.

Under development since May 2020 and __not stable__ yet.

## Installation
* Python libraries in [the requirements file](requirements.txt)
* WeazyPrint may require [additional installation steps](https://weasyprint.readthedocs.io/en/stable/install.html)
* Plotly.IO further requires the [Plotly Orca App](https://github.com/plotly/orca)

## Examples

Look in
* 'examples/PyReports_examples.py' for the Python program.
* 'examples/PyReports_examples.pdf' for a PDF report rendition
* 'examples/PyReports_examples.html' for a HTML report rendition

## Documentation

__DRAFT__

### A) Create a Report Container

    from pyreports import PyReports

    <container_name> = PyReports("My_Title",
                                 "path/to/my/folder",
                                 description = "This is a PyReport description!",
                                 settings = {'some_option': 'some_value'}
                                )

Arguments:

* __container__ (mandatory, string).  
Container name, i.e filename.

* __path__ (mandatory, string).  
Path to save the report.

* __title__ (optional, string).  
Container title, i.e. report title. Supports Markdown.

* __description__ (optional, string).  
Free text displayed after the report title. Supports Markdown.

* __settings__ (optional, dictionary).  
Container settings. See below for the possible key/values.

    * __logging_level__ (string, default = 'INFO', values = 'DEBUG'|'INFO'|'WARNING').  
    Controls what level of information you want in your log: DEBUG; INFO, WARNING, ERROR, CRITICAL.

    * __stylesheets__ (string|list-of-strings|dictionary, default is ['style', 'style_phuse'] ).  
    Select style(s) to apply to your report. This can either be a [PyReports style name](./css), css file, or a list that combines both. See ["Alternate stylesheets"](./stylesheets.md) for details on specifying alternate style sheets in a dictionary.

    * __sas_encoding__ (string, default = 'utf-8').  
    Encoding used to read SAS datasets

    * __date_format__ (string, default = '%b %d %Y %H:%M:%S').  
    Use strftime syntax from datetime module (https://strftime.org/)

    * __values_alignment__ (string, values = 'left'|'right', default = 'right').  
    How to align values in the table.

    * __labels_alignment__: (string, values = 'left'|'right'|'center', default = 'center').  
    How to align label columns.

    * __toc__: (boolean, default = True).  
    Display a table of contents.


### B) Add a table to the container

    <container_name>.add_table(data,
                               filter,
                               columns,
                               title,
                               subtitle,
                               description,
                               footnotes,
                               remap,
                               settings)

Arguments:

* __data__ (mandatory, pandas Dataframe|string).  
Input data. You can pass directly a pandas Dataframe or the path/filename for a supported file (SAS7BDAT, Feather).

* __filter__ (optional, string).  
Pattern for filtering the input Dataframe. Use Dataframe.query() method.

* __columns__ (optional, dictionary|list|string).  
Columns used in the table and how to use them.

    * If a dictionary is provided, a nested dictionary is expected to provide info on that column. Example:

        { '<column_name>'  : {'usage': 'display' } }

        Here are the possible info you can provide:

        * __usage__: String. How the use the column. Default='display'. Values = display|group|order|across.
            * display > column is simply displayed
            * group   > column is used for group-by
            * order   > lexicograph ordering of the column values.
            * across  > transpose column values and frequency count
        * __label__: String. Column name to use in the table header.
        * __digits__: Integer. How many digits should be displayed. Only for numeric columns.
        * __width__: Integer. How many characters should be displayed. Only for text columns.
        * __ascending__: Boolean. Default = True. Sorting order when usage = 'order' or 'group'.
        * __remap__: String (key from the remap argument).
        * __compute__: String. Basic derivation to create a new column. Example: "WEIGHT / (HEIGHT**2)". Variable names must exist in the data or be computed before.

    * If a list is provided, it's assumed that the list contains column names. A default behavior will be used for these.
    * If a string is provided, it's assumed that the string is a space-separated list of column names. A default behavior will be used for these.

* __title__ (optional, string).  
Table title. Support Markdown. If not provided PyReport will assign a default title, e.g. "Untitled table #3".

* __subtitle__ (optional, string).  
Table title. Support Markdown.

* __description__ (optional, string).  
Table description. Support Markdown.

* __footnotes__ (optional, list|string).  
Table footnotes. You can pass a single string or a list of strings. Support Markdown.

* __remap__ (optional, dictionary|pandas dataframe).  
Object for remapping column values.

    * If the object is a dictionary, it is expected to follow this syntax:

        {'<remap_name>': {'<coded_value1>': '<decode_value1>', '<coded_value2>': '<decode_value2>'} }
        * Use <remap_name> in the column definition, e.g. {'colA', 'usage': 'display', remap='<remap_name>'}
        * The coded values are found in the data
        * The decoded values will be displayed in the table.

    * If the object is a pandas dataframe, it will be converted. By default the following 3 columns are expected in this df: 'ct', 'old' and 'new'.
    If you dataframe is using different column names, you can map them with the table setting _remap_cols_.

* __settings__  (optional, dictionary).  
Dictionary of table settings

    * __display_date__ (boolean, default = False).  
    Should we display a footer date after that table?

    * __groupby_on_same_table__ (boolean, default = False).  
    True: Group values on the same table. This essentialy creates a hierarchical dataframe index and outer group(s) is/are sparsified.
    False: Create subtables for each group.

    * __page_break__ (boolean, default = False).  
    When groupby_on_same_table=False, should I insert a page break after each group/subtable?

    * __show_index__ (boolean, default = False).  
    Should I display the dataframe index in the table? In certain circomstances I will force this to True.

    * __max_rows__ (integer).  
    Cap the number of rows displayed in the table

    * __remap_cols__ (dictionary).
    If the remap object is a pandas dataframe, we need to convert it to a dict. To do that, we expect the df to have 3 columns: 'ct', 'old' and 'new'.
    If the df has different column names, use remap_cols to map them
        {'my_ct':  'ct' ,
        'my_old': 'old',
        'my_new': 'new'}


### C) Add a figure to the container

    <container_name>.add_figure( figure
                                ,title
                                ,subtitle
                                ,description
                                ,footnotes
                                ,settings)

Arguments:

* __figure__ (mandatory, Plotly object).
Pass a Plotly figure object.

* __title__ (optional, string).
Figure title. Support Markdown. If not provided PyReport will assign a default title, e.g. "Untitled figure #3"

* __subtitle__ (optional, string).
Figure title. Support Markdown.

* __description__ (optional, string).
Figure description. Support Markdown.

* __footnotes__ (optional, list|string)
Figure footnotes. You can pass a single string or a list of strings. Support Markdown.

* __settings__  (optional, dictionary).
Dictionary of figure settings. See [plotly.io.write_image](https://plotly.com/python-api-reference/generated/plotly.io.write_image.html) for valid settings.


### D) Export the container

    <container_name>.export(output_formats,
                            settings)

Arguments:

* __output_formats__ (optional, list|string, default = 'PDF', values = 'HTML' |'PDF').  
PDF:  saves the report as a new PDF file,
HTML: saves the report as a new HTML file. Opened straight away by your web browser if 'open_in_browser' = True.
You can pass a list of all the formats you want.
Files are saved in <path.container>

* __settings__ (optional, dictionary).  
Dictionary of export settings.

    * __open_in_browser__ (boolean, default = False).  
    Should I open the HTML page in your Web browser?

## To do list

* Take care of the TEMPORARY notes in the code
* Fancy documentation in HTML
* Certain settings could be applied to the container (e.g. groupby_on_same_table) and table settings could overide that
* New container settings: print the log in the report Y/N
* Filtering using other methods than df.query() ?
* In table:
    * Summarization rows column-wise
    * Summarization columns element-wise
    * re-think summarization column column-wise to maybe allow to order,group,display,across them. Combined with functional programming thoughts.
* Improve report layout (CSS !)
* header/footer styles + page numbers (weazyprint seems limiting this)
* Support CSV & XLSX as input data formats
* Test JSON as input
* Support output formats RTF and DOCX
* Re-enable legacy syntax for columns? Not too sure anymore.
* Once stable: package it for Python package repo: https://packaging.python.org/tutorials/packaging-projects/

## Authors
* **Nicolas Dupuis** - *Initial design and work* - [nicolasdupuis](https://gitlab.com/nicolasdupuis)

## Thanks
* Inspiration: https://pbpython.com/pdf-reports.html
* Proc report doc: http://www.nebsug.org/wp-content/uploads/2014/05/Cochran_Proc_Report1.pdf
* The CSS stylesheet comes from http://www.blueprintcss.org/

## License
GPL V.3