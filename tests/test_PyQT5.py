# Copied from
# https://stackoverflow.com/a/63290662

import os, os.path as op
import sys, re
from PyQt5 import QtWidgets, QtWebEngineWidgets
from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QPageLayout, QPageSize
from PyQt5.QtWidgets import QApplication

import logging
import datetime

logging.basicConfig( filename=op.join(sys.path[0], f"{re.sub('py$', 'log', op.basename(sys.argv[0]), flags=re.I)}")
                    ,filemode="w"
                    ,format  ="%(levelname)8s - %(module)16s > %(funcName)14s: %(message)s"
                    ,level   ="INFO")

logging.info(f"Starting log at {datetime.datetime.now(datetime.timezone.utc)}")
logging.info(f"Working directory is {os.getcwd()}")
logging.info(f"Calling script is {sys.argv[0]}, in {sys.path[0]}")
logging.info("-" * 100)

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    loader = QtWebEngineWidgets.QWebEngineView()
    loader.setZoomFactor(1)
    layout = QPageLayout()
    layout.setPageSize(QPageSize(QPageSize.Letter))
    layout.setOrientation(QPageLayout.Landscape)
    loader.load(QUrl('https://mozilla.org'))
    loader.page().pdfPrintingFinished.connect(lambda *args: QApplication.exit())

    def emit_pdf(finished):
        loader.page().printToPdf("test.pdf", pageLayout=layout)

    loader.loadFinished.connect(emit_pdf)
    sys.exit(app.exec_())