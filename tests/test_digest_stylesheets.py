# Authors: Dante Di Tommaso
# Licensed under GPL V3
# https://gitlab.com/nicolasdupuis/pyreports

import unittest

import os.path as op
import os, sys, re, logging, datetime as dt, urllib.request as ureq
from pathlib import Path

#sys.path.append(".")
#import utilities

#
# Tests for digest_stylesheets function
#
# Standardize structure of user settings to a uniform dict with
# keys of
#   "", "Preferred", "Alternate-stylesheet-title", ...
# and values of lists of dictionaries with details for component stylesheets
#   [  {  "url"    : "fully-qualified path to sheet, URLs or filenames"
#       , "file"   : "filename.css, to store contents for HTML reports as {path}/css/filename.css"
#       , "content": "CSS content for {path}/css/filename.css"
#      }
#    , ...
#   ]
# "file" and "content" keys are only required for HTML targets that require creation of local {path}/css/ files
#

# Location constants

calling_pgm = op.abspath(sys.argv[0])
calling_dir = op.abspath(sys.path[0])
util_path   = op.abspath(op.dirname(utilities.__file__))
pr_path     = re.sub("utilities$", "", util_path)

# Initialize test objects and PyReports styles
issues=[]
expect_dict = {}

with open(op.join(pr_path, 'tests', 'extra_style.css'), 'r') as csssrc:
    extra_style_css = csssrc.read()

with ureq.urlopen('https://www.w3.org/StyleSheets/Core/Modernist') as csssrc:
    w3c_modern_css = csssrc.read().decode()

with ureq.urlopen('https://www.w3.org/StyleSheets/Core/Swiss') as csssrc:
    w3c_swiss_css = csssrc.read().decode()

# Set Up Resources to Populate Test Cases
# -- for local file, relative to report
expect_dict["style"] = { "url"    : "css/style.css"
                        ,"file"   : "style.css"
                        ,"content": utilities.style_css
                        }

expect_dict["style_phuse"] = { "url"    : "css/style_phuse.css"
                              ,"file"   : "style_phuse.css"
                              ,"content": utilities.style_phuse_css
                              }

expect_dict["style_phuse_dkgreen"] = { "url"    : "css/style_phuse_dkgreen.css"
                                      ,"file"   : "style_phuse_dkgreen.css"
                                      ,"content": utilities.style_phuse_dkgreen_css
                                      }

expect_dict["style_phuse_ltgreen"] = { "url"    : "css/style_phuse_ltgreen.css"
                                      ,"file"   : "style_phuse_ltgreen.css"
                                      ,"content": utilities.style_phuse_ltgreen_css
                                      }

expect_dict["w3c_modern"] = { "url"    : "css/Modernist.css"
                             ,"file"   : "Modernist.css"
                             ,"content": w3c_modern_css
                             }

expect_dict["w3c_swiss"] = { "url"    : "css/Swiss.css"
                            ,"file"   : "Swiss.css"
                            ,"content": w3c_swiss_css
                            }

expect_dict["extra_style"] = { "url"    : "css/extra_style.css"
                              ,"file"   : "extra_style.css"
                              ,"content": extra_style_css
                              }


# -- for referenced files, skipping local file

expect_dict["u_style"] = { "url"    : Path(op.join(pr_path, "css", "style.css")).as_uri()
                          ,"file"   : ""
                          ,"content": ""
                          }

expect_dict["u_style_phuse"] = { "url"    : Path(op.join(pr_path, "css", "style_phuse.css")).as_uri()
                                ,"file"   : ""
                                ,"content": ""
                                }

expect_dict["u_style_phuse_dkgreen"] = { "url"    : Path(op.join(pr_path, "css", "style_phuse_dkgreen.css")).as_uri()
                                        ,"file"   : ""
                                        ,"content": ""
                                        }

expect_dict["u_style_phuse_ltgreen"] = { "url"    : Path(op.join(pr_path, "css", "style_phuse_ltgreen.css")).as_uri()
                                        ,"file"   : ""
                                        ,"content": ""
                                        }

expect_dict["u_w3c_modern"] = { "url"    : "https://www.w3.org/StyleSheets/Core/Modernist"
                               ,"file"   : ""
                               ,"content": ""
                               }

expect_dict["u_w3c_swiss"] = { "url"    : "https://www.w3.org/StyleSheets/Core/Swiss"
                              ,"file"   : ""
                              ,"content": ""
                              }

expect_dict["u_extra_style"] = { "url"    : Path(op.join(pr_path, "tests", "extra_style.css")).as_uri()
                                ,"file"   : ""
                                ,"content": ""
                                }


# Set Up User-Settings (US) to process (correctly :)
# Plus   Expected-Settings, what the digest_stylesheets() call should return

us01 = {"stylesheets": { "": ['style', {"url": 'style_phuse'} ]
                        ,"preferred": {"url": 'extra_style.css'}
                        ,"W3C Modernist": 'https://www.w3.org/StyleSheets/Core/Modernist'
                        ,"W3C Swiss Style": [ '../css/style_phuse_dkgreen.css'
                                             ,{"url": "https://www.w3.org/StyleSheets/Core/Swiss"}
                                             ]
                        }
        }

ex01 = {"": [ expect_dict["style"]
             ,expect_dict["u_style_phuse"]
            ]
        ,"Preferred":       [expect_dict["u_extra_style"]]
        ,"W3C Modernist":   [expect_dict["w3c_modern"]]
        ,"W3C Swiss Style": [ expect_dict["style_phuse_dkgreen"]
                             ,expect_dict["u_w3c_swiss"]
                             ]
        }


us02 = {'stylesheets': { "": 'style'
                        ,'PhUSE Dark Green': ['style_phuse', '../css/style_phuse_dkgreen.css']
                        ,'W3C Core Swiss': 'https://www.w3.org/StyleSheets/Core/Swiss'
                        }
        }

ex02 = { "": [expect_dict["style"]]
        ,"Preferred": []
        ,"PhUSE Dark Green": [ expect_dict["style_phuse"]
                              ,expect_dict["style_phuse_dkgreen"]
                              ]
        ,"W3C Core Swiss": [expect_dict["w3c_swiss"]]
        }


# Test these
def compare_dicts() :
    for key, val in results.items() :
        ''' Key is name of a EXPECTED RESULT dict. Val is (result dict, issues list) tuple from digest_stylesheets'''
        ex = eval(key)
        dg = val[0]
        logging.info(f"--- Comparing Expected Results for '{key}'. Cycling through actual results dictionary.")
        for ky, vl in dg.items() :
            try :
                if ex[ky] != dg[ky] :
                    logging.warning(f"FAIL on expected result '{ky}'.")
                    logging.warning(f"-- EXPECTED result  '{ex[ky]}'.")
                    logging.warning(f"-- Actual result is '{dg[ky]}'.")
                else :
                    logging.info(f"PASS on result '{ky}'. ")
            except :
                logging.warning(f"Unable to check EXPECTED value for Actual result key '{ky}' is '{vl}'.")

results = {}

issues = []
results["ex01"] = utilities.digest_stylesheets(us01['stylesheets'], issues)

issues = []
results["ex02"] = utilities.digest_stylesheets(us02['stylesheets'], issues)

compare_dicts()

''' TEST in interactive session

settings = {}
def set_dicts () :
    global one, two, u_two, tre, settings
    one   = {"url": "one", "file": "one.css", "content":"css for one"}
    two   = {"url": "two", "file": "two.css", "content":"css for two"}
    u_two = {"url": "u_two", "file": "", "content":""}
    tre   = {"url": "tre", "file": "tre.css", "content":"css for tre"}
    settings['stylesheets'] = {"": [one], "Preferred": [two, u_two], "Three": [tre]}

# List comprehensions

# Stylesheet titles
[key for key, val in settings['stylesheets'].items()]

# Nested list comprehensions

# All local /css/ files, cycle through each declared Stylesheet Title
#
# Filename.css for /css/ folder
[ [itm['file'] for itm in val if '' != itm['file']]
  for key, val in settings['stylesheets'].items() ]

# Content for /css/filename.css
[ [itm['content'] for itm in val if '' != itm['file']]
  for key, val in settings['stylesheets'].items() ]

# URLs for PDFs - Persistent and Preferred styles, only
[ [itm['url'] for itm in val ]
  for key, val in settings['stylesheets'].items() if key in ['', 'Preferred'] ]

'''
