# Authors: Dante Di Tommaso
# Licensed under GPL V3
# https://gitlab.com/nicolasdupuis/pyreports


from pathlib import Path
import sys, os, os.path as op, re, urllib3
import utilities

# Verify sources and Standardize structure of settings['stylesheet'] dictionary.
# Keys will be HTML style Titles, including literal "" and "Preferred" titles:
#   "", "Preferred", "Alternate-style-title", "..."
# Each value will uniformly be a list of 3-key dictionaries with details for component style sheets
#   [  {  "url"    : "fully-qualified path to this sheet - a URL or filename"
#       , "file"   : "filename.css, to store contents for HTML reports as {path}/css/filename.css"
#       , "content": "CSS content for {path}/css/filename.css"
#      }
#    , ...
#   ]
# "file" and "content" keys are only required for HTML targets with creation of local {path}/css/ files

def digest_stylesheets(user_style, issues) :
    style_dict = {  "": []
                    , "Preferred": []}

    sp0 = sys.path[0]
    opd = op.abspath(op.dirname(utilities.__file__))

    def __append_style(stitle, sstr, sfile=True) :
        '''Append a PyReports style to Title stitle ("", "Preferred", ...).
        To create /css/ external file for HTML reports, sfile must be TRUE '''
        valid_style = 0
        bn  = op.basename(sstr)
        spj = op.join(sp0, sstr)

        if 'preferred' == stitle.casefold() :
            stitle = 'Preferred'

        # File and Content remain null, to link to file, rather than write /css/ copy
        sdfile = ''
        sdcontent = ''

        # Initialize the standardized style dictionary, if it does not exist
        try :
            isinstance(style_dict[stitle], list)
        except :
            style_dict[stitle] = []

        if "".join([sstr, "_css"]) in dir(utilities) :
            # PyReports style name ... convert to location, /css/filename and content
            sdurl = os.sep.join([ "".join([re.sub("utilities$", "", opd), "css"])
                                 ,".".join([sstr, "css"])
                                 ])
            sdurl = Path(sdurl).as_uri()

            if sfile :
                # Do create /css/ external file
                sdurl     = "/".join(["css", ".".join([sstr, "css"])])
                sdfile    = ".".join([bn, "css"])
                sdcontent = eval(f"utilities.{sstr}_css")
            valid_style = 1

        elif op.isfile(spj) :
            # File, abs path or relative to calling script ... convert
            sdurl = op.abspath(spj)
            sdurl = Path(sdurl).as_uri()

            if sfile :
                # Do create /css/ external file
                sdurl     = "/".join(["css", bn])
                sdfile    = bn
                sdcontent = open(spj).read()
            valid_style = 1

        else :
            # URL for CSS stylesheet ... store if user requests external file for HTML
            sdurl = sstr

            try :
                with urllib3.PoolManager() as csssrc:
                    if sfile:
                        sdurl     = "".join(chr for chr in bn if (chr.isalnum() or chr in "-._~#[]@!$&'()+,;="))
                        sdurl     = "/".join(["css", ".".join([re.sub("\.css$", "", sdurl), "css"])])
                        sdfile    = op.basename(sdurl)
                        sdcontent = csssrc.request('GET', sstr).data.decode()
                    valid_style = 1

            except:
                issues.append(f"[stylesheets setting] Ignoring {sstr}. Not a valid PyReports style name, filename or stylesheet URL.")

        if valid_style :
            style_dict[stitle].append({ "url"    : sdurl
                                       ,"file"   : sdfile
                                       ,"content": sdcontent
                                       })

    def __append_url_style(ustitle, usdict):
        ''' Extract single "url" key, and pass string to __append_style '''
        for uky, uvl in usdict.items() :
            if "url" == uky.lower() :
                # Expect only string or list of strings
                if isinstance(uvl, str) :
                    __append_style(ustitle, uvl, sfile=False)
                elif isinstance(uvl, list) :
                    for ulst in uvl :
                        __append_style(ustitle, ulst, sfile=False)
                else :
                    issues.append(f"[stylesheets setting] Ignoring unexpected key value for key '{uky}'. Expecting only string(s).")
            else :
                issues.append(f"[stylesheets setting] Unexpected key '{uky}' in '{ustitle}' style. Read the docs for settings['stylesheets'].")


    # Main processing of user's stylesheets setting
    #   A string, list of strings, or stylesheets dictionary with keys "", "Preferred" or other
    if isinstance(user_style, str) :
        __append_style("", user_style)

    elif isinstance(user_style, list) :
        for item in user_style :
            # Expect strings, only
            try :
                __append_style("", item)
            except TypeError :
                issues.append(f"[stylesheets setting] Ignoring unexpect type {type(item)} in stylesheets list. Read the docs for settings['stylesheets'].")

    elif isinstance(user_style, dict) :
        # dict with keys of style sheet titles
        for dkey, dval in user_style.items() :
            # dkey is the HTML stylesheet Title, dval lists the sheet(s)
            if isinstance(dval, str) :
                __append_style(dkey, dval)
            elif isinstance(dval, list) :
                for litm in dval:
                    # Expect string, or dict with single "url" key
                    if isinstance(litm, str) :
                        __append_style(dkey, litm)
                    elif isinstance(litm, dict) :
                        __append_url_style(dkey, litm)
            elif isinstance(dval, dict) :
                # dict with single "url" key
                __append_url_style(dkey, dval)

            else :
                issues.append(f"[stylesheets setting] Ignoring unexpected key value for key '{dkey}'. Read the docs for settings['stylesheets'].")
    else :
        issues.append(f"[stylesheets setting] Ignoring unexpect stylesheets setting of type {type(user_style)}. Read the docs for settings['stylesheets'].")

    return(style_dict, issues)