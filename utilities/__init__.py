# Authors: Nicolas Dupuis
# Licensed under GPL V3
# https://gitlab.com/nicolasdupuis/pyreports

# Import all utilities
#
# ... EXCEPT .digest_arguments which may be obsolete - mentions add_report() which does not exist
#

from .convert_input_data import convert_input_data
# from .digest_arguments import digest_arguments
from .digest_request import digest_request
from .digest_stylesheets import digest_stylesheets
from .validate_settings import validate_settings
from .css_styles import style_css, style_phuse_css, style_phuse_dkgreen_css, style_phuse_ltgreen_css, style_stripe_css

# Purpose is to restrict import to these elements
# https://docs.python.org/3/tutorial/modules.html#importing-from-a-package
__all__ = [  "convert_input_data"
#            , "digest_arguments"  # Obsolete - mentions non-existent add_report()?
           , "digest_request"
           , "digest_stylesheets"
           , "validate_settings"
           , "style_css"
           , "style_phuse_css"
           , "style_phuse_dkgreen_css"
           , "style_phuse_ltgreen_css"
           , "style_stripe_css"
           ]
