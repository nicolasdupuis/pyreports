# Authors: Nicolas Dupuis
# Licensed under GPL V3
# https://gitlab.com/nicolasdupuis/pyreports

import pandas as pd

def convert_input_data(data):

    ''''    
        Convert the input data and return a pandas Dataframe
        Supported input formats for conversion: SAS7BDAT, feather, JSON.
    '''

    # User gave us a string, let's assume it's a filename
    if isinstance(data, str):
            
            # If file attribute is sas7bdat, then let's try read_sas()
            if data.split('.')[-1].lower() == '.sas7bdat':

                try: 
                        df = pd.read_sas(data, encoding=self.container_settings['sas_encoding'])
                        df.name = data.split('.sas7bdat')[0]
                        logging.info('SAS input dataset was successfully converted to a pandas Dataframe.')
                        return df
                except:
                        msg = f"Failed to locate or read the SAS dataset '{data}'."
                        logging.error(msg, exc_info=True)
                        raise Exception (msg)

            # If file attribute is feather, then let's try read_feather()
            if data.split('.')[-1].lower() == '.feather':

                try: 
                        df = pd.read_feather(data)
                        df.name = data.split('.sas7bdat')[0]
                        logging.info('Feather input file was successfully converted to a pandas Dataframe.')
                        return df
                except:
                        msg = f"Failed to locate or read the feather file '{data}'."
                        logging.error(msg, exc_info=True)
                        raise Exception (msg)

            # If file attribute is JSON, then let's try read_json()
            if data.split('.')[-1].lower() == '.json':

                try: 
                        df = pd.read_json(data)
                        df.name = data.split('.sas7bdat')[0]
                        logging.info('JSON input file was successfully converted to a pandas Dataframe.')
                        return df
                except:
                        msg = f"Failed to locate or read the JSON file '{data}'."
                        logging.error(msg, exc_info=True)
                        raise Exception (msg)

            else:
                msg = f"Input data format is not recognized/supported."
                logging.error(msg)
                raise Exception (msg)

    else: 
            msg = f"Input data format is not recognized/supported."
            logging.error(msg)
            raise Exception (msg)        