# Instantiate PyReports styles, written out to /css/ folder for HTML outputs
# 1. default style_css (for HTML output: <path>/css/style.css)
# 2. style_phuse_css (for HTML output: <path>/css/style_phuse.css)
# 3. style_phuse-light_css (for HTML output: <path>/css/style_phuse-light.css)
# 4. style_stripe_css (for HTML output: <path>/css/style_stripe.css)

with open('css/style.css', 'r') as css_file:
    style_css = css_file.read()

with open('css/style_phuse.css', 'r') as css_file:
    style_phuse_css = css_file.read()

with open('css/style_phuse_dkgreen.css', 'r') as css_file:
    style_phuse_dkgreen_css = css_file.read()

with open('css/style_phuse_ltgreen.css', 'r') as css_file:
    style_phuse_ltgreen_css = css_file.read()

with open('css/style_stripe.css', 'r') as css_file:
    style_stripe_css = css_file.read()
