# Authors: Nicolas Dupuis
# Licensed under GPL V3
# https://gitlab.com/nicolasdupuis/pyreports

import pandas as pd
import logging
import re
from markdown import markdown


def digest_arguments(columns, defines, remap, title, data, description, footnotes, table_settings):

    '''
        Crunch the arguments passed by user to add_report(), return a dictionary with digested info.
    '''

    # Parsing function, using regex
    def parse_define(_string, pattern, occurence=1):

        found = re.findall(pattern, _string)
        
        if len(found) > occurence:
            msg = f'Bad syntax found in {_string}'
            logging.error(msg)
            raise Exception(msg)
        
        elif len(found) == 1: 
            return found[0]

    # Initialize variables
    report = {}
    report['column'] = {}
    report['order'], report['groups'], report['analysis'], report['labels'] = [], [], [], {}
    report['order'] = {'columns': [], 'ascending': []}

    # Analyse argument 'title'
    #------------------------------------------------------------------------------------------------------------------------------------------------

    # If missing, try instead the dataframe name, or use a default name
    if title == None:
        
        try: 
            report['title'] = data.name
        except:
            report['title'] = 'Untitled #' + str(report_id)
    else: 
        report['title'] = title

    logging.info(f"--------------------------------------------------------------------")
    logging.info(f"Adding report '{report['title']}'")

    # Analyse argument 'columns'
    #------------------------------------------------------------------------------------------------------------------------------------------------
    
    # If user passed a string, let's convert that to a list. Expected delimiter is a space.
    if isinstance(columns, str):
        columns = columns.split(' ')

    elif isinstance(columns, list):
        pass

    # If user didn't pass the argument or passed something else than a string or a list, use all the column names from the data instead
    else:

        report['show_index'] = True
        report['default_columns'] = True
        
        columns = list(data.columns)
        #for column in list(data.columns):
        #    report['column'][column] = {'stat':None}
        
        logging.info("Argument columns was missing or using a wrong format, I'll use the data columns.")       

    # Let's analyse what we got in 'columns'
    for i, column in enumerate(columns):

        # Check if a summarization function was given, e.g. "AGE,mean"
        try: 
            variable, stat = column.split(',')
            stat = stat.lower()
            
            if stat not in supported_stats:
                logging.warning(f"Statistics for column '{col}'' is not supported. Defaulting to mean.")
                stat = 'mean'

        except:
            variable = column
            stat = None
        
        # Check if the column is in the data
        if variable in data:
            report['column'][variable] = {'stat':stat}
        else:
            logging.warning(f"Column '{variable}' is not in the input data. I'll ignore that you asked for it.")

    report['columns'] = list(report['column'].keys()) # convenience
    logging.debug(f"Columns= {report['columns']}")

    # Initialize more
    for column in report['columns']:
        for item in ['width', 'digit', 'remap']:
            report['column'][column][item] = None

    # Analyse argument 'defines'
    #------------------------------------------------------------------------------------------------------------------------------------------------

    # If missing, use all the columns we have, vanilla style
    if defines == None:
        for variable in report['columns']:
            report['column'][variable]['define'] = 'display'

    # Analyze each define we got
    else:

        for i, define in enumerate(defines):

            define_variable = re.findall(r'^\w*', define)[0]

            if define_variable in report['columns']:

                # Is it defined as "display"?
                if re.search(r'''(\w*)\s*[/]\s*display''', define):
                    report['column'][define_variable]['define'] = 'display'

                # Is it defined as "group"?
                elif re.search(r'''(\w*)\s*[/]\s*group''', define):
                    report['column'][define_variable]['define'] = 'group'                        
                    report['groups'].append(define_variable)

                # Is it defined as "analysis"?
                elif re.search(r'''(\w*)\s*[/]\s*analysis''', define):

                    for column in columns:
                        if column.split(',')[0] == variable:
                            report['column'][define_variable]['stat'] = column.split(',')[1]
                    
                    report['column'][define_variable]['define'] = 'analysis'  

                # Is it defined as "order"?
                elif re.search(r'''(\w*)\s*[/]\s*order''', define):
                    report['column'][define_variable]['define'] = 'order' 

                # Do we have variable with a label?
                label = parse_define(define, '''['"](.*?)['"]''')
                if label != None:
                    report['labels'][define_variable] = label

                # Do we have column widths?
                width = parse_define(define, r'''width\s*=\s*(.*)''')
                if width != None: 
                    report['column'][define_variable]['width'] = int(width)

                # Do we have a 'remap'?
                r = parse_define(define, r'''remap\s*=\s*(.*)''')
                if r != None: 
                    report['column'][define_variable]['remap'] = r

                # Do we have precision digits?
                digit = parse_define(define, r'''digits\s*=\s*(.*)''')
                if digit != None:
                    report['column'][define_variable]['digit'] = int(digit)

                # Do we have ordering (group or order)?
                if report['column'][define_variable]['define'] in ['order', 'group']: 
                
                    report['order']['columns'].append(define_variable)

                    if 'descending' in define.lower():
                            report['order']['ascending'].append(False)
                    else:
                            report['order']['ascending'].append(True)
    

        # All variable in argument 'columns' should have a definition in the 'defines' argument. Use default if not.
        not_defined = []
        for column in report['columns']:
            if 'define' not in report['column'][column]:
                not_defined.append(column)
                report['column'][column]['define'] = 'display'
        if len(not_defined) > 0:
            logging.warning(f"The following columns are required but not defined: {' '.join(not_defined)}.")

    
    # Analyse argument 'remap'
    #------------------------------------------------------------------------------------------------------------------------------------------------

    # Supported formats: Dict and pandas Dataframes
    if isinstance(remap, dict):
            logging.debug('remap object is a dictionary.')

    elif isinstance(remap, pd.DataFrame): # we received a Dataframe, let's convert it to a dict
        
            try: 
                _remap = {}

                # if user provided a dict to rename the cols in 'remap', then let's rename
                if 'rename_remap' in settings:
                        remap = remap.rename(columns = settings['rename_remap'])

                # let's convert the df into a Dict
                for name, group in remap.groupby(['ct']):
                        _remap[name] = {key:value for (key,value) in zip(remap['old'], remap['new'])}
                
                logging.debug("remap object was converted from pandas Dataframe to a dictionary.")
                report['remap'] = _remap

            except:
                logging.warning("Failed to convert the remap Dataframe. Attempts to remap variables will not succeed.")

    elif remap == None:
            report['remap'] = {}

    else: # Unsupported format
            logging.warning("Your remap object is not supported. Attempts to remap variables will not succeed.")
            report['remap'] = {}

    # Analyse argument 'description'
    #------------------------------------------------------------------------------------------------------------------------------------------------

    # Convert argument description (a string in Markdown) to a markdown object
    if isinstance(description, str):
        report['description'] = markdown(description)
    
    elif description != None: 
        logging.warning(f"Unsupported format for argument description.")
        report['description']= None

    else:
        report['description']= None

    # Analyse argument 'footnotes'
    #------------------------------------------------------------------------------------------------------------------------------------------------
    if isinstance(footnotes, str):
        report['footnotes'] = [markdown(footnotes)]
    
    elif isinstance(footnotes, list):
        report['footnotes'] = [markdown(footnote) for footnote in footnotes]
    
    else: 
        report['footnotes'] = None

    # Log all of this
    #------------------------------------------------------------------------------------------------------------------------------------------------
    for column in report['column']:
        logging.debug(f"Column '{column}': {report['column'][column]}.")
    if len(report['groups'])   > 0: logging.info(f"Groups = {report['groups']}")
    if len(report['analysis']) > 0: logging.info(f"Analysis = {report['analysis']}")
    if len(report['order'])    > 0: logging.info(f"Order = {report['order']}")
    if len(report['remap'])    > 0: logging.info(f"remap = {report['remap']}")

    return report
