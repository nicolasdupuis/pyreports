# Authors: Nicolas Dupuis
# Licensed under GPL V3
# https://gitlab.com/nicolasdupuis/pyreports

import pandas as pd
import logging
import re
from markdown import markdown
import utilities

def no_p_markdown(non_p_string) -> str:
    ''' Strip enclosing paragraph marks, <p> ... </p>, 
        which markdown() forces, and which interfer with some jinja2 layout
    '''
    return re.sub("(^<P>|</P>$)", "", markdown(non_p_string), flags=re.IGNORECASE)

def digest_request(request, data_columns, id):

    '''
        Crunch the arguments passed by user, return a dictionary with digested info.
    '''

    # Initialize variables
    #------------------------------------------------------------------------------------------------------------------------------------------------    
    digest = {}
    digest['column'], digest['labels'], digest['analysis'] = {}, {}, {}
    digest['compute'], digest['categories'] = [], []
    digest['rename'] = {}    
    digest['orders'] = {'columns': [], 'ascending': []}

    usages = ['display', 'group', 'order', 'across']
    for usage in usages: 
        digest[usage] = []

    supported_stats = ['min', 'max', 'mean', 'median', 'count'] # TEMPORARY I don't like this
    column_details = ['categories', 'input', 'label', 'compute', 'width', 'digits', 'remap', 'ascending'] 


    # Analyse argument 'title' & 'subtitle'
    #------------------------------------------------------------------------------------------------------------------------------------------------

    # If title is missing, use a default name. Convert to Markdhown otherwise
    if request['title'] == None:
        digest['title'] = no_p_markdown('Untitled table #' + str(id))
        digest['title_raw'] = 'Untitled table #' + str(id)

    else: 
        digest['title'] = no_p_markdown(request['title'])
        digest['title_raw'] = request['title']

    # Start logging
    logging.info("-" * 100)
    logging.info(f"Analyzing arguments for {request['type']} '{digest['title_raw']}'...")

    # Convert subtitle to Markdown if provided.
    if request['subtitle'] != None: 
        digest['subtitle'] = no_p_markdown(request['subtitle'])
    else: 
        digest['subtitle'] =  None


    # Analyze argument 'description'
    #------------------------------------------------------------------------------------------------------------------------------------------------

    # Convert argument description (a string in Markdown) to a markdown object
    if isinstance(request['description'], str):
        digest['description'] = no_p_markdown(request['description'])
        digest['description'] = no_p_markdown(digest['description'])
    
    elif request['description'] != None: 
        logging.warning(f"Unsupported format for argument description.")
        digest['description']= None

    else:
        digest['description']= None

    
    # Analyze argument 'footnotes'
    #------------------------------------------------------------------------------------------------------------------------------------------------
    if isinstance(request['footnotes'], str):
        digest['footnotes'] = [markdown(request['footnotes'])]
    
    elif isinstance(request['footnotes'], list):
        digest['footnotes'] = [markdown(footnote) for footnote in request['footnotes']]
    
    else: 
        digest['footnotes'] = None


    # If the item is a Figure, we can leave now. The rest will for tables
    if request['type'] == 'figure':
        return digest


    # Load argument 'filter' into {digest}
    #------------------------------------------------------------------------------------------------------------------------------------------------
    digest['filter'] = request['filter']

    
    # Load argument 'columns' into {digest}
    #------------------------------------------------------------------------------------------------------------------------------------------------

    # If user passed a dictionary, we load its content
    if isinstance(request['columns'], dict):

        # List of all the columns (convenience)
        digest['columns'] = list(request['columns'].keys())

        # Load column info
        for column in digest['columns']:
            
            # TEMPORARY: to do > lower case every key
            digest['column'][column] = request['columns'][column]

    # Columns is not a dict, let's have a look
    # TEMPORARY: that's where we should plug the legacy syntax digest
    else: 

        digest['show_index'] = True
        digest['default_columns'] = True
        
        # If user pased a list or a string, we can try to make sense of it
        if isinstance(request['columns'], str) or isinstance(request['columns'], list):
        
            if isinstance(request['columns'], str):
                digest['columns'] = request['columns'].split(' ')
            else: 
                digest['columns'] = request['columns']

        else: # User passed None or something unexpected, we'll use the columns from the actual data

            digest['columns'] = data_columns
            logging.info("Argument 'columns' was missing or using a wrong format. I'll use all the columns from the input data with a default behavior.")   

        # Set default behavior for all columns
        for column in digest['columns']:
            digest['column'][column] = {}
            digest['column'][column]['usage'] = 'display'       


    # Analyze what we loaded in {digest}
    #------------------------------------------------------------------------------------------------------------------------------------------------
    
    for column in digest['columns']:

        # Set 'target' variable. Use the column neme if not provided
        if 'input' not in digest['column'][column]:
            digest['column'][column]['input'] = column        
   
        # Check existence of the input variable in the input data (unless it's a column we need to compute)
        if (digest['column'][column]['input'] not in data_columns) and ('compute' not in digest['column'][column]):
            logging.warning(f"Column '{column}' does not exist in the input data. I'll ignore it.")
            del digest['column'][column]
            digest['columns'] = digest['columns'].remove(column)

        # For each column, look at each key we got in the details dictionary (e.g. usage, label, digits etc)
        for key in digest['column'][column].keys():

            # key='Usage': check the validity
            if key == 'usage':

                if digest['column'][column][key] not in usages + supported_stats:
                    logging.warning(f"Unexpected key in column '{column}': '{digest[column][key]}'. I'll use default.")
                    digest[column]['usage'] = 'display'

                # Create digest['analysis'] for that column= input var: sum_function
                if digest['column'][column][key] in supported_stats:
                    digest['analysis'][digest['column'][column]['input']] = digest['column'][column][key]

                    digest['rename'][digest['column'][column]['input']]= column

            elif key in column_details:
                pass # TEMPORARY what's that?

            else: 
                logging.warning(f"Unexpected key in column '{column}': '{key}'. I'll use default.")
                digest['column'][column]['usage'] = 'display'
        
        # Set usage to 'display' if missing
        if 'usage' not in digest['column'][column]:
            digest['column'][column]['usage'] = 'display'

        # Convenience
        for usage in usages: 
            if usage == digest['column'][column]['usage']:
                digest[usage].append(column)      

        if 'label' in digest['column'][column]:
            digest['labels'][column] = digest['column'][column]['label']

        if 'compute' in digest['column'][column]:
            digest['compute'].append(column)

        if 'categories' in digest['column'][column]:
            digest['categories'].append(column)            

        # Do we have ordering (group or order)?
        if digest['column'][column]['usage'] in ['order','group']: 
            if 'ascending' not in digest['column'][column]:
                digest['column'][column]['ascending'] = True

            else:
                if digest['column'][column]['ascending'] not in [True, False]:
                    logging.warning(f"Unexpected value for 'ascending' in column '{column}'. I'll use default.")
                    digest['column'][column]['ascending'] = True

    # Convenience on order, first deal with the groups then with the other ordered vars: 
    for column in digest['columns']:
        if digest['column'][column]['usage'] in ['group']: 
            digest['orders']['columns'].append(column)
            digest['orders']['ascending'].append( digest['column'][column]['ascending'] )

    for column in digest['columns']:
        if digest['column'][column]['usage'] in ['order']: 
            digest['orders']['columns'].append(column)
            digest['orders']['ascending'].append( digest['column'][column]['ascending'] )


    # Analyze argument 'remap'. Supported formats: Dict and pandas Dataframes
    #------------------------------------------------------------------------------------------------------------------------------------------------

    # We got a dict, easy.
    if isinstance(request['remap'], dict):
            digest['remap'] = request['remap']

    # We got a Dataframe, let's convert it to a dict
    elif isinstance(request['remap'], pd.DataFrame):
        
            try: 
                remap = {}

                # User provided a dict to rename the columss in 'remap', so let's rename
                if 'remap_cols' in request['settings']:
                        request['remap'] = request['remap'].rename(columns = request['settings']['remap_cols'])

                # let's convert the df into a Dict
                for name, group in request['remap'].groupby(['ct']):
                        remap[name] = {key:value for (key,value) in zip(request['remap']['old'], request['remap']['new'])}
                
                logging.debug("remap object was converted from pandas Dataframe to a dictionary.")
                digest['remap'] = remap

            except:
                digest['remap'] = {}
                logging.warning("Failed to convert the remap Dataframe. Attempts to remap variables will not succeed.")

    # We got nothing...
    elif request['remap'] == None:
            digest['remap'] = {}

    # Unsupported format
    else: 
            logging.warning("Your remap object is not supported. Attempts to remap variables will not succeed.")
            digest['remap'] = {}

    
    
    # Log all this
    #------------------------------------------------------------------------------------------------------------------------------------------------

    logging.debug(f"Columns= {digest['columns']}")
    for column in digest['columns']:
        logging.debug(f"Column '{column}': {digest['column'][column]}.")
    
    if len(digest['display'])  > 0: logging.info(f"Display  = {digest['display']}")
    if len(digest['group'])    > 0: logging.info(f"Groups   = {digest['group']}")
    if len(digest['analysis']) > 0: logging.info(f"Analysis = {digest['analysis']}")
    if len(digest['across'])   > 0: logging.info(f"Across   = {digest['across']}")
    if len(digest['compute'])  > 0: logging.info(f"Compute  = {digest['compute']}")
    if len(digest['categories'])  > 0: logging.info(f"Categorical  = {digest['categories']}")
    if len(digest['orders']['columns']) > 0: logging.info(f"Orders  = {digest['orders']}")
    

    # Checks
    #------------------------------------------------------------------------------------------------------------------------------------------------

    if len(digest['across']) > 1:
        msg = "Sorry, cannot have more than one 'across' variable!"
        logging.error(msg)
        raise Exception (msg)

    if (len(digest['display']) > 0 or len(digest['order']) > 0) and len(digest['across']) > 0:
        msg = "Sorry, cannot have 'display' or 'order' variables AND 'across' variables at the same time!"
        logging.error(msg)
        raise Exception (msg)


    # Job's done
    #------------------------------------------------------------------------------------------------------------------------------------------------
    return digest