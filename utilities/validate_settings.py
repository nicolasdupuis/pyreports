# Authors: Nicolas Dupuis
# Licensed under GPL V3
# https://gitlab.com/nicolasdupuis/pyreports

from .digest_stylesheets import digest_stylesheets

def validate_settings(user_settings, valid_settings):

    '''
        For each global setting, see if values are valid. Reset value to default if necessary and complain.
    '''

    settings, issues = {}, []

    # Look at all the possible settings
    for setting in valid_settings:

        # Verify source and standardize structure of stylesheets settings
        if "stylesheets" == setting :
            settings[setting], issues = digest_stylesheets(user_settings[setting], issues)

        # User gave us a value for that setting
        elif setting in user_settings:

            # There's a constraint on the value
            if 'valid_values' in valid_settings[setting]: 

                # Valid value, we'll take it
                if user_settings[setting] in valid_settings[setting]['valid_values']:
                    settings[setting] = user_settings[setting]
                
                # Invalid value, we'll use the default 
                else: 
                    settings[setting] = valid_settings[setting]
                    issues.append(f"Setting '{setting}' is invalid. I'm reseting to default value, i.e. {valid_settings[setting]['default']}")

            # no constraints, let's make sure at least the object type is right (except if default is None)
            else: 
                if valid_settings[setting]['default'] != None and type(user_settings[setting]) != type(valid_settings[setting]['default']):
                    settings[setting] = valid_settings[setting]['default']
                    issues.append(f"Setting '{setting}' is invalid. I'm reseting to default value, i.e. {valid_settings[setting]['default']}")
                
                else: 
                    settings[setting] = user_settings[setting]

        # Otherwise let's take the default one, silently 
        else: 
            settings[setting] = valid_settings[setting]['default']

    return (settings, issues)