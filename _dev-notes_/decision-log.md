# Roadmap
* Begin with Narratives
  * __[ND] action:__  Schedule meeting between Sumesh, Andre, Nico, Dante (, and Olivier?) to outline and align on objectives, establish collaboration agreement, forum for progress reporting
  * __[ND] action:__  Add a _Janssen_ narrative shell to [our Table Style reference](../references/PyReports_Table-Model-and-Style.docx)
  * __[DDT] action:__  Add a _Sanofi_ narrative shell to [our Table Style reference](../references/PyReports_Table-Model-and-Style.docx)
  * __[DDT] action:__  Progress the style tasks mentioned in [our To-Do list](./_TODO_.md)
* Investigate the WeazyPrint dependency in our enterprise environments
  * WeazyPrint > cairo > GTK+
    * __Note:__ GIMP2 is available in Sanofi, and installs libcairo-2.dll ... could be sufficient
  * Plotly.IO > Orca app
## Narrative Focus (short-term)
  * Considerations:
    1. __Internal audience vs. public disclosure:__ _privacy issue_, potentially a report setting: Include/Exclude patient-idenfitying details such as race, ethnicity, exact age, country ...
    2. 
## Investigate "grammar or tables" for next version (long-term)
  * [Atorus' Tplyr](https://github.com/atorus-research/Tplyr)
  * [RStudio's grammar of tables](https://gt.rstudio.com/)
  * RStudio provide a map/model of table elements (class definition)
  * Atorus rely on PhUSE recommended displays, incl. in our "reference" folder
