* Simple stuff:
  * \[__DDT__\] to implement stylesheet UI (OK, not so simple, afterall)
    * __DONE:__ multiple stylesheets, 
    * __DONE:__ active vs. alternate, 
    * __DONE:__ using "ref" and "title" attributes correctly
    * __TO-DO:__ pass "Persistent" and "Preferred" stylesheets to PDF export
  * \[X\] Scripting options seem limited (see below), as far as option to script MS WinWord to convert pdf or html to docx. LibreOffice seems best alternative, and is more active than Apache OpenOffice.
  * \[X\] Pandas dataframe.to_html - even with options formatters=, styles= - seem too limited for the satisfying formats that PyReports should be able to produce. See comments, below, and escpecially [PyReports Table Model and Style.docx](../references/PyReports_Table-Model-and-Style.docx) and the related [example_of_styles.html](../css/example_of_styles.html).

* __Q:__ Can PyReports customize HTML table structure, and then CSS style these structures?
  * \[__DDT__\] Updated [PyReports Table Model and Style.docx](../references/PyReports_Table-Model-and-Style.docx)
    * Three tables from [01-PhUSE-WP_Analyses-and-Displays-Associated-with-Demographics-Disposition-Medications.pdf](../references/01-PhUSE-WP_Analyses-and-Displays-Associated-with-Demographics-Disposition-Medications.pdf) demonstrate the control that PyReports would need over HTML _structure_ in order to apply non-trivial CSS _styles_.
    * The hand-made HTML [example_of_styles.html](../css/example_of_styles.html) demonstrates 3 related CSS styles (based on HTML table structures)
      * these styles are defined in [css/style_phuse.css](../css/style_phuse.css).
      * <code>&lt;TABLE&gt;&lt;TR&gt;&lt;TD>, etc.</code> - Default PhUSE table style
      * <code>&lt;TBODY CLASS=SUBTOT_BLOCK&gt;</code> - Category Grouping, with __bold first row__ , and indented row-labels on remaining rows in block
      * <code>&lt;TBODY CLASS=ROWSPAN_BLOCK&gt;</code> - Row labels spanning all rows in a block, with a bold first line in block.
  * \[__ND__\] To create example dataframes for similar tables, so that \[__DDT__\] can further investigate how PyReports can recognize such table requests and style them accordingly.
    * See further notes, below, about options for styling PyReports tables.
  * __Tentative conclusion (DDT):__ Initial investigation suggests that we need to use [pandas style API](https://pandas.pydata.org/pandas-docs/stable/user_guide/style.html) ... but this may not entirely address the issue of HTML structure (e.g., rowspan, colspan)
    * <code>_formatters_</code> option of [pandas Dataframe.to_html](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_html.html) seems insufficient, since it applies style to all __columns__ by name or position.
      * _Requires further investigation._
    * <code>_classes_</code> option of [pandas Dataframe.to_html](https://code-examples.net/en/q/1200676) seems insufficient, since it applies to an entire table.
      * _Requires further investigation._
    * [pandas style API](https://pandas.pydata.org/pandas-docs/stable/user_guide/style.html) may give sufficient control of CSS styling, but the issue of HTML structure remains ...
      * _Potential rabbit hole, but potentially powerful, essential rabbit-hole._

* __Q:__ Decision on DOCX approach for now?
  1. \[__ND__\] to note in [decision-log.md](./decision-log.md) those PyPI packages that he already tested for DOCX output, but without satisfying results
      * [Pandocs](https://pandoc.org/MANUAL.html#general-options) seems to support DOCX target, among many others ...
      * ... [PyPandocs ???](https://pypi.org/project/pypandoc/)
      * [Python-Docx ???](https://pypi.org/project/python-docx/)
      * another?

  1. Script [MSWord](https://support.microsoft.com/en-us/office/command-line-switches-for-microsoft-office-products-079164cd-4ef5-4178-b235-441737deb3a6#ID0EAABAAA=Word) or [LibreOffice](https://en.wikipedia.org/wiki/LibreOffice) or [Apache OpenOffice](https://en.wikipedia.org/wiki/Apache_OpenOffice) from a satisfying PDF or HTML report?
      * __Note:__ On Win10, the following [MS WinWord.exe](https://support.microsoft.com/en-us/office/command-line-switches-for-microsoft-office-products-079164cd-4ef5-4178-b235-441737deb3a6#ID0EAABAAA=Word) command lines produce UNsatisfying results:
      * <code>"%ProgramFiles%\Microsoft Office\root\Office16\WINWORD.EXE" __/F__ examples\PyReports_examples.html</code>
      * <code>"%ProgramFiles%\Microsoft Office\root\Office16\WINWORD.EXE" __/F__ examples\PyReports_examples.pdf</code>
  2. Set aside requirement for now, advise users to explore manual efforts on their systems to import/convert pdf or html
  3. Try harder - dig deeper into pandas, styling api, [Pandocs](https://pandoc.org/MANUAL.html#general-options), ... ???