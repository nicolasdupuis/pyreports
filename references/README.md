# PhUSE Standard Analyses & Code Sharing Working Group
* White papers based on industry and health authority collaboration.  
* [See White Papers & References published as PhUSE Deliverables](https://www.phuse.eu/white-papers)

These table and listing layouts are the basis of [style_phuse*.css](../css/)

The CRAN gt "grammar of tables" documentation  presents a compelling model for building tables by compiling layers off information.