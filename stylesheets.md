### Alternate Stylesheets

PyReports adopts HTML conventions for alternate style sheets. Based on a title attribute, style sheets are either [Persistent, Preferred or Alternate](https://developer.mozilla.org/en-US/docs/Web/CSS/Alternative_style_sheets).

* __Persistent__ style sheets "always applies to the document." Use the literal null title `""`.
* __Preferred__ style sheets are "applied by default, but disabled if an alternate stylesheet is selected." Use the literal title `"Preferred"`.
* __Alternate__ style sheets are "disabled by default, can be selected in browser." Use any other unique title. __Alternate__ styles apply to dynamic HTML reports, only.

### Cumulative style sheets
If your report requires only one PyReports style, or one custom style sheet, [the basic __stylesheets__ setting](https://gitlab.com/nicolasdupuis/pyreports#a-create-a-report-container) suffices: pass in a PyReports style name, or a stylesheet filename or a list of both.

To specify alternate style sheets, the `stylesheets` setting should be a dictionary that groups style sheets by title (the key). A string indicates a single style sheet. A list-of-strings inidicates cumulative style sheets:

    { "single": "style-name",
      "multiple": ["style-name", "/path-to/sillywalk-styles.css"]
      ... }

Note the two literal titles `""` and `"Preferred"`, above, and their meanings.

Examples of settings for `stylesheets` follow.

__Replace__ the default, persistent style with a string:

    "/path-to/sillywalk-styles.css"

__Augment__ the default, persistent style with a list of strings:

    ["style_phuse", "/path-to/sillywalk-styles.css"]

__Define preferred and alternate__ style sheets with a dictionary:

    { "Preferred": "path-to/shrubbery-styles.css",
      "Silly Walk": "/path-to/sillywalk-styles.css",
      ... }

__Augment with preferred and alternate__ style sheets, which, when active, supplement the persistent style. `""` and `"Preferred"`, as noted above, are literal titles:

    { "": "style_phuse",
      "Preferred": "path-to/shrubbery-styles.css",
      "Silly Walk": "/path-to/sillywalk-styles.css",
      ... }

### HTML local versus linked style sheets

For HTML reports, by default, PyReports creates local, external style sheets in a `/css/` folder alongside your report.
* This preserves the current styles for the report, and supports packaging the report for sharing.
* Styles with redundant titles will overwrite prior `/css/` files. To avoid this, use unique filenames.

Alternatively, PyReports can link to an external style sheet, without preserving it in the HTML report's `/css/` folder. This is suitable if the report should always reflect the current version of a published style sheet such as one of the [W3C Core Styles](https://www.w3.org/StyleSheets/Core/).

You must use the alternate style sheets form, above, for the `stylesheets` setting: a dictionary that groups style sheets by title (the key).

Use a one-key dictionary instead of a string, within a value of a `stylesheets` dictionary, to flag a style sheet for linked reference. The one key is lower-case "url":
  * "url", with a string value of a fully-qualified path, not relative to the report

PyReports interprets the `stylesheets` setting `{"url": "/path-to/sillywalk-styles.css"}` as an alternate style titled "url", with no persistent or preferred style. If you instead want a persistent linked style sheet, use the `stylesheets` setting `{"": {"url": "/path-to/sillywalk-styles.css"}}`.

Further examples of `stylesheets` settings dictionaries:

    { "": "style_phuse",
      "Preferred": {"url": "/path-to/sillywalk-styles.css"},
      "W3C Swiss Style": {"url": "https://www.w3.org/StyleSheets/Core/Swiss"}
    }

    { "": "style_phuse",
      "Preferred": [ "path-to/shrubbery-styles.css",
                      {"url": "/path-to/sillywalk-styles.css"}
                   ],
      "W3C Swiss Style": {"url": "https://www.w3.org/StyleSheets/Core/Swiss"}
    }

* URL paths should be absolute, and not dependent on the location of the HTML report.

### PDF
For PDF outputs, only persistent and preferred style sheets are applied.

